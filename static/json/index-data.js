module.exports.json = {
  "list": [{
      "icon": "transfer-text",
      "title": "站在未来的十字路口",
      "color": "red",
      "type": "outline",
      "page": "out"
    },
    {
      "icon": "report-problem",
      "title": "微信小程序入门教程之一：初次上手",
      "color": "orange",
      "type": "outline",
      "page": "ruanyifeng-01"
    },
    {
      "icon": "discover",
      "title": "微信小程序入门教程之二：页面样式",
      "color": "green",
      "type": "outline",
      "page": "ruanyifeng-02"
    },
    {
      "icon": "imac",
      "title": "微信小程序入门教程之三：脚本编程",
      "color": "yellow",
      "type": "outline",
      "page": "ruanyifeng-03"
    },
    {
      "icon": "sticker",
      "title": "微信小程序入门教程之四：API使用",
      "color": "blue",
      "type": "outline",
      "page": "ruanyifeng-04"
    },
    {
      "icon": "link",
      "title": "微信小程序入门教程之五：扩展框架",
      "color": "purple",
      "type": "outline",
      "page": ""
    },
    {
      "icon": "link",
      "title": "微信小程序入门教程之六：基础信息",
      "color": "purple",
      "type": "outline",
      "page": ""
    },
    {
      "icon": "link",
      "title": "微信小程序入门教程之七：配置文件",
      "color": "purple",
      "type": "outline",
      "page": ""
    },
    {
      "icon": "link",
      "title": "微信小程序入门教程之八：合法域名",
      "color": "purple",
      "type": "outline",
      "page": ""
    },
    {
      "icon": "link",
      "title": "微信小程序入门教程之九：云开发",
      "color": "purple",
      "type": "outline",
      "page": ""
    },
    {
      "icon": "link",
      "title": "微信小程序入门教程之十：其他框架",
      "color": "purple",
      "type": "outline",
      "page": ""
    },
    {
      "icon": "link",
      "title": "自我总结",
      "color": "purple",
      "type": "outline",
      "page": "selfSummary"
    },
    {
      "icon": "link",
      "title": "婚姻是爱情的坟墓",
      "color": "purple",
      "type": "outline",
      "page": "loveGrave"
    }
  ]
}